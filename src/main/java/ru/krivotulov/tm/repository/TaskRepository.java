package ru.krivotulov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.krivotulov.tm.api.repository.ITaskRepository;
import ru.krivotulov.tm.model.Task;

import java.util.*;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public Task create(@NotNull final String userId,
                       @NotNull final String name
    ) {
        final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public Task create(@NotNull final String userId,
                       @NotNull final String name,
                       @NotNull final String description
    ) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId,
                                         @NotNull final String projectId
    ) {
        return models
                .stream()
                .filter(model -> projectId.equals(model.getProjectId()) &&
                        userId.equals(model.getUserId()))
                .collect(Collectors.toList());
    }

}
