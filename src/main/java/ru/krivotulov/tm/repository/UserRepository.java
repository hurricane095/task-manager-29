package ru.krivotulov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.api.repository.IUserRepository;
import ru.krivotulov.tm.enumerated.Role;
import ru.krivotulov.tm.model.User;
import ru.krivotulov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * UserRepository
 *
 * @author Aleksey_Krivotulov
 */
public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return models
                .stream()
                .filter(model -> login.equals(model.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        return models
                .stream()
                .filter(model -> email.equals(model.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull final String email) {
        return findByEmail(email) != null;
    }

}
