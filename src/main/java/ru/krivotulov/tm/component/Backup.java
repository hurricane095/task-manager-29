package ru.krivotulov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.krivotulov.tm.command.data.DataBackupSaveCommand;

/**
 * Backup
 *
 * @author Aleksey_Krivotulov
 */
public class Backup extends Thread {

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull Bootstrap bootstrap){
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (true){
            Thread.sleep(3000);
            bootstrap.runCommand(DataBackupSaveCommand.NAME, false);
        }
    }

}
