package ru.krivotulov.tm.exception.system;

import ru.krivotulov.tm.exception.AbstractException;

/**
 * PermissionException
 *
 * @author Aleksey_Krivotulov
 */
public final class PermissionException extends AbstractException {

    public PermissionException() {
        super("Error! Permission is incorrect...");
    }

}
