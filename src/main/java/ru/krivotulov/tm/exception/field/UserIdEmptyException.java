package ru.krivotulov.tm.exception.field;

/**
 * UserIdEmptyException
 *
 * @author Aleksey_Krivotulov
 */
public final class UserIdEmptyException extends AbstractFieldException {

    public UserIdEmptyException() {
        super("Error! User id is empty...");
    }
}
