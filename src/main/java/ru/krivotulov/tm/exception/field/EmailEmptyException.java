package ru.krivotulov.tm.exception.field;

/**
 * LoginEmptyException
 *
 * @author Aleksey_Krivotulov
 */
public final class EmailEmptyException extends AbstractFieldException {

    public EmailEmptyException() {
        super("Error! email is empty...");
    }

}
