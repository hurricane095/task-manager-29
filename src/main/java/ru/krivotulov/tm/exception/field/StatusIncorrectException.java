package ru.krivotulov.tm.exception.field;

/**
 * StatusIncorrectException
 *
 * @author Aleksey_Krivotulov
 */
public final class StatusIncorrectException extends AbstractFieldException{

    public StatusIncorrectException() {
        super("Error! Status is incorrect...");
    }
}
