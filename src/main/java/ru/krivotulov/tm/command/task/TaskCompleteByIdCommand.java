package ru.krivotulov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.enumerated.Status;
import ru.krivotulov.tm.util.TerminalUtil;

/**
 * TaskCompleteByIdCommand
 *
 * @author Aleksey_Krivotulov
 */
public class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-complete-by-id";

    @NotNull
    public static final String DESCRIPTION = "Complete task by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID: ");
        @Nullable final String id = TerminalUtil.readLine();
        @NotNull final String userId = getUserId();
        getTaskService().changeStatusById(userId, id, Status.COMPLETED);
    }

}
