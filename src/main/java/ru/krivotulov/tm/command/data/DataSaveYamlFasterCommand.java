package ru.krivotulov.tm.command.data;

import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.krivotulov.tm.dto.Domain;

import java.io.FileOutputStream;

/**
 * DataSaveYamlFasterCommand
 *
 * @author Aleksey_Krivotulov
 */
public class DataSaveYamlFasterCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-yaml-faster";

    @NotNull
    public static final String DESCRIPTION = "Save data in yaml file faster.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA YAML FASTER SAVE]");
        @NotNull final Domain domain = getDomain();
        @NotNull final YAMLMapper yamlMapper = new YAMLMapper();
        @NotNull final String json = yamlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull FileOutputStream fileOutputStream = new FileOutputStream(FILE_FASTER_YAML);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

}
