package ru.krivotulov.tm.command.data;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.krivotulov.tm.dto.Domain;

import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * DataLoadXmlFasterCommand
 *
 * @author Aleksey_Krivotulov
 */
public class DataLoadXmlFasterCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-xml-faster";

    @NotNull
    public static final String DESCRIPTION = "Load xml faster.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA XML FASTER LOAD]");
        @NotNull byte[] bytesJson = Files.readAllBytes(Paths.get(FILE_FASTER_JSON));
        @NotNull final String json = new String(bytesJson);
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final Domain domain = xmlMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

}
