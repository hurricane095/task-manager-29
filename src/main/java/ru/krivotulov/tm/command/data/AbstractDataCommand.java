package ru.krivotulov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.api.service.*;
import ru.krivotulov.tm.command.AbstractCommand;
import ru.krivotulov.tm.dto.Domain;
import ru.krivotulov.tm.enumerated.Role;

/**
 * AbstractDataCommand
 *
 * @author Aleksey_Krivotulov
 */
public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public static final String FILE_BINARY = "./data.bin";

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public static final String FILE_JAXB_XML = "./data_jaxb.xml";

    @NotNull
    public static final String FILE_JAXB_JSON = "./data_jaxb.json";

    @NotNull
    public static final String FILE_FASTER_XML = "./data_faster.xml";

    @NotNull
    public static final String FILE_FASTER_JSON = "./data_faster.json";

    @NotNull
    public static final String FILE_FASTER_YAML = "./data_faster.yaml";

    @NotNull
    public static final String JAVAX_XML_BIND_CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    public static final String ORG_ECLIPSE_PERSISTENCE_JAXB_JAXBCONTEXT_FACTORY = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    public static final String APPLICATION_JSON = "application/json";

    @NotNull
    public Domain getDomain() {
        @NotNull Domain domain = new Domain();
        domain.setProjects(getProjectService().findAll());
        domain.setTasks(getTaskService().findAll());
        domain.setUsers(getUserService().findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        getProjectService().set(domain.getProjects());
        getTaskService().set(domain.getTasks());
        getUserService().set(domain.getUsers());
        getAuthService().logout();
    }

    @NotNull
    public ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    @NotNull
    public IProjectService getProjectService() {
        return serviceLocator.getProjectService();
    }

    @NotNull
    public IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    @NotNull
    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    @NotNull
    public IPropertyService getPropertyService() {
        return serviceLocator.getPropertyService();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMINISTRATOR};
    }

}
