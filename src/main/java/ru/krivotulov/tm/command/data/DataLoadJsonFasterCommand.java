package ru.krivotulov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.krivotulov.tm.dto.Domain;

import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * DataLoadJsonFasterCommand
 *
 * @author Aleksey_Krivotulov
 */
public class DataLoadJsonFasterCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-json-faster";

    @NotNull
    public static final String DESCRIPTION = "Load json faster.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA JSON FASTER LOAD]");
        @NotNull byte[] bytesJson = Files.readAllBytes(Paths.get(FILE_FASTER_JSON));
        @NotNull final String json = new String(bytesJson);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

}
